package com.zhjhserver.utils;

import org.apache.mina.core.buffer.IoBuffer;


public class DataEncyt {
	private int _encyKey;
	private int _adder;
	private int _multiper;

	public DataEncyt(int adder,int multiper) {
		updateEncytKey();
	}
	public DataEncyt(){
		_adder = 0x7abcdef7;
		_multiper = 1051;
	}
	public void resetEncytKey(){
		_encyKey = 0;
	}
	public int updateEncytKey(){
		_encyKey = ((~_encyKey) + _adder) * _multiper;
		_encyKey = _encyKey ^ (_encyKey >> 16);
		return _encyKey;
	}
	/**
	 *得到生成的加密KEY
	 */
	public int getEncytKey(){
		return _encyKey;
	}

	/**
	 *移位加解密算法
	 */
	public  IoBuffer encryption(IoBuffer buf) {
		byte[] bytes = buf.array();
		IoBuffer encytArray   = IoBuffer.allocate(PackData.DATA_MAXIMUM);
		for(int i = 0;i < buf.limit();i++) {
			encytArray.put((byte)(bytes[i] ^ ((_encyKey++ & (0xff << 16)) >> 16)));
		}
		return encytArray;
	}
}
