package com.zhjhserver.utils;

import java.util.ArrayList;

public class Room
{
    private static final int TABLE_NUM = 100;

    private Hall mHall = null;
    private int mIndex = 0;
    private ArrayList<Table> mTableList = null;

    public Room(Hall hall, int index)
    {
        mHall = hall;
        mIndex = index;

        mTableList = new ArrayList<Table>();

        for (int i=0; i<TABLE_NUM; i++)
        {
            Table table = new Table(this, i);
            mTableList.add(table);
        }
    }

    public int getIndex()
    {
        return mIndex;
    }

    public Seat fastFindSeat()
    {
        Seat seat = null;

        for (Table table: mTableList)
        {
            seat = table.fastFindSeat();

            if (seat != null)
            {
                break;
            }
        }

        return seat;
    }
}
