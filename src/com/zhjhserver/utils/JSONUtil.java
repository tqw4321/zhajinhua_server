package com.zhjhserver.utils;

import net.sf.json.JSONObject;

public class JSONUtil
{
    public static Object getValue(JSONObject obj, String key)
    {
    	if (obj == null)
    		return null;

    	if (!obj.containsKey(key))
    		return null;

    	return obj.get(key);
    }

    public static String getString(JSONObject obj, String key)
    {
    	if (obj == null)
    		return null;

    	if (!obj.containsKey(key))
    		return null;

    	return obj.getString(key);
    }

    public static int getInt(JSONObject obj, String key)
    {
    	if (obj == null)
    		return 0;

    	if (!obj.containsKey(key))
    		return 0;

    	return obj.getInt(key);
    }

    public static boolean getBoolean(JSONObject obj, String key)
    {
    	if (obj == null)
    		return false;

        if (!obj.containsKey(key))
    		return false;

    	return obj.getBoolean(key);
    }
}
