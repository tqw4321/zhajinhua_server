package com.zhjhserver.utils;

import java.sql.ResultSet;
import java.sql.SQLException;

import net.sf.json.JSONObject;

import org.apache.mina.core.session.IoSession;

import com.zhjhserver.database.UserDb;

public class Player
{
    public interface Status
    {
        public static final int UNRIPE = 0;     // 未准备
        public static final int IDLE = 1;       // 在观看(已就座)
        public static final int READY = 2;      // 已准备
        public static final int PLAYING = 3;    // 正在玩中
    }

    private IoSession mSession = null;
    private Seat mSeat = null;

    private String mUsername = null;
    private String mNickname = null;
    private int mMoney = 0;     // 金币数量
    private int mLevel = 0;     // 等级, grade
    private int mCard1 = 0;     // 牌1
    private int mCard2 = 0;     // 牌2
    private int mCard3 = 0;     // 牌3

    private int mStatus = 0;    // 状态
    private boolean mIsRobot = false;   // 是否机器人
    private boolean mIsView = false;    // 是否看牌
    private String mIcon = null;
    private String mSex = null;

    private String mDeviceID = null;
    private String mDeviceModel = null;
    private String mCreateTime = null;
    private String mLastTime = null;

    public Player(IoSession session, String username)
    {
        mSession = session;
        mUsername = username;

        ResultSet rs = UserDb.getInfo(username);

        try {
			if (rs!=null && rs.next())
			{
				mNickname = rs.getString("nickname");
				mDeviceID = rs.getString("device");
				mDeviceModel = rs.getString("model");
				mCreateTime = rs.getString("create_time");
				mLastTime = rs.getString("last_time");
				mSex = rs.getString("sex");
				mIcon = rs.getString("icon");
				mMoney = rs.getInt("score");
				mLevel = rs.getInt("grade");
				mIsRobot = rs.getBoolean("is_robot");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

        if (PubFunc.isEmptyString(mNickname))
        {
        	if (!PubFunc.isEmptyString(mDeviceModel))
        		mNickname = mDeviceModel;
        	else
        		mNickname = mUsername;
        }
    }

    public void logon()
    {
        UserDb.setLogon(mUsername);
    }

    public void logout()
    {
        setSeat(null);
    }

    public IoSession getSession()
    {
        return mSession;
    }

    public void setSeat(Seat seat)
    {
        if (mSeat != null)
        {
            mSeat.setPlayer(null);
        }

        mSeat = seat;

        if (mSeat != null)
        {
            mSeat.setPlayer(this);
        }
    }

    public Seat getSeat()
    {
        return mSeat;
    }

    public int getMoney()
    {
        return mMoney;
    }

    public String getUsername()
    {
    	return mUsername;
    }

    public int getLevel()
    {
        return mLevel;
    }

    public int getStatus()
    {
        return mStatus;
    }

    public String getInfo()
    {
        JSONObject jsonOut = new JSONObject();
        jsonOut.put("username", mUsername);
        jsonOut.put("nickname", mNickname);
        jsonOut.put("status", mStatus);
        jsonOut.put("money", mMoney);
        jsonOut.put("level", mLevel);
        jsonOut.put("is_view", mIsView);
        jsonOut.put("is_robot", mIsRobot);

        if (mSeat != null)
        {
        	jsonOut.put("seat_id", mSeat.getIndex());
        }
        else
        {
        	jsonOut.put("seat_id", -1);
        }

        return jsonOut.toString();
    }

    public void idle()
	{
		mStatus = Status.IDLE;
	}

	public void ready()
	{
		mStatus = Status.READY;
	}

	public void play()
	{
		mStatus = Status.PLAYING;
	}
}
