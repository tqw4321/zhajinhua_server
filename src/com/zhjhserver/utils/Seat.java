package com.zhjhserver.utils;

import org.apache.mina.core.session.IoSession;

public class Seat
{
    private Table mTable = null;
    private int mIndex = 0;
    private Player mPlayer = null;

    public Seat(Table table, int index)
    {
        mTable = table;
        mIndex = index;
    }

    public int getIndex()
    {
        return mIndex;
    }

    public Table getTable()
    {
        return mTable;
    }

    public void setPlayer(Player player)
    {
        synchronized(this)
        {
            mPlayer = player;
        }
    }

    public Player getPlayer()
    {
        synchronized(this)
        {
            return mPlayer;
        }
    }

    public boolean isIdle()
    {
        synchronized(this)
        {
            return (mPlayer == null);
        }
    }

    public String getPlayerInfo()
    {
        synchronized(this)
        {
            if (mPlayer == null)
                return null;
            else
                return mPlayer.getInfo();
        }
    }
}
