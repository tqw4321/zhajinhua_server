package com.zhjhserver.utils;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.sf.json.JSONObject;

public class PubFunc{

    public static String getNowTime()
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String datetime = sdf.format(new Date());

		return datetime;
    }

	public static String stringForTime(long timeMs){
		SimpleDateFormat formatter = new SimpleDateFormat("HH:mm:ss");
		String str = formatter.format(timeMs);
		return str;
	}

	public static String toMd5(String str)
    {
		byte[] bytes = null;

		try {
			bytes = str.getBytes("UTF-8");
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
			return "";
		}

        try {
            MessageDigest algorithm = MessageDigest.getInstance("MD5");
            algorithm.reset();
            algorithm.update(bytes);
            return toHexString(algorithm.digest(), "");
        } catch (NoSuchAlgorithmException e) {
            ////Log.v("he--------------------------------ji", "toMd5(): " + e);
            //throw new RuntimeException(e);
            e.printStackTrace();
            return "";
        }
    }

    private static String toHexString(byte[] bytes, String separator)
    {
        StringBuilder hexString = new StringBuilder();
        String s;

        for (byte b : bytes)
        {
        	s = Integer.toHexString(0xFF & b);
        	if (s.length()==1) s = "0" + s;
            hexString.append(s).append(separator);
        }

        return hexString.toString();
    }

    // \\u604b\\u7231\\u5047\\u671f-01
	public static String decodeUnicodeHex(String txt)
	{
		//if (!txt.contains("\\u"))
		//{
		//	return txt;
		//}

		int len = txt.length();
		int pos = 0;
		char[] chars = new char[len];
		//StringBuffer outBuffer = new StringBuffer(len);
		char c;
		int i = 0;

		while (i < len-4)
		{
			c = txt.charAt(i++);

			if (c == '\\')
			{
				c = txt.charAt(i++);

				if (c == 'u')
				{
					String hex = txt.substring(i, i+4);

					try
					{
						int ic = Integer.parseInt(hex, 16);
						chars[pos++] = (char)ic;
						//outBuffer.append((char)ic);
						i += 4;
					}
					catch (Exception e)
					{
						chars[pos++] = '\\';
						chars[pos++] = 'u';
						//outBuffer.append("\\u");
					}
				}
				else
				{
					chars[pos++] = '\\';
					chars[pos++] = c;
					//outBuffer.append('\\');
					//outBuffer.append(c);
				}
			}
			else
			{
				chars[pos++] = c;
				//outBuffer.append(c);
			}
		}

		while (i < len)
		{
			c = txt.charAt(i++);
			chars[pos++] = c;
			//outBuffer.append(c);
		}

		return new String(chars, 0, pos);
		//return outBuffer.toString();
	}

    public static String formatCurrentTime()
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date curDate = new Date(System.currentTimeMillis());
        String str = formatter.format(curDate);

        return str;
    }

    public static String formatOffsetTime(int dayOffset)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd 00:00:00");
        Date date = new Date(System.currentTimeMillis() - dayOffset*24*3600*1000);
        String str = formatter.format(date);

        return str;
    }

    public static String removeHeadTailChar(String txt, char c)
    {
    	int left = 0;
    	int len = txt.length();
    	int right = len;

    	if (txt.charAt(0) == c)
    	{
    		left++;
    	}

    	if (txt.charAt(len-1) == c)
    	{
    		right--;
    	}

    	return txt.substring(left, right);
    }

    //递归删除文件及文件夹
    public static void deleteDirs(File file) {
        if (file.isFile()) {
            file.delete();
            return;
        }

        if(file.isDirectory()){
            File[] childFiles = file.listFiles();
            if (childFiles == null || childFiles.length == 0) {
                file.delete();
                return;
            }

            for (int i = 0; i < childFiles.length; i++) {
                deleteDirs(childFiles[i]);
            }

            file.delete();
        }
    }

    public static boolean isEmptyString(String str)
    {
    	if (str == null)
    		return true;

    	if (str.length() == 0)
    		return true;

    	return false;
    }
}
