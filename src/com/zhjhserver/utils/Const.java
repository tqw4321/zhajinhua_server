package com.zhjhserver.utils;

public class Const
{
	// 处理代码
		public static final int ERROR_SUCCESS = 0;             // 成功
		public static final int ERROR_INVALID_PARAM   = -1;    // 无效参数
		public static final int ERROR_INVALID_NAME    = -2;    // 无效用户名
		public static final int ERROR_INVALID_PASSWD  = -3;    // 无效密码
		public static final int ERROR_NAME_EXISTED    = -4;    // 用户名已存在
		public static final int ERROR_DATABASE_QUERY  = -5;    // 数据库查询出错
		public static final int ERROR_UNKOWN  = -6;    // 未知原因
		public static final int ERROR_NO_SEAT  = -7;    // 没有座位了
		public static final int ERROR_MUL_LOGON  = -8;    // 重复登录

	    // functoin id len
	    public static final int FUNC_ID_LEN = 4;

	    // function id:
	    public static final int FUNC_REGISTER = 1;          // 注册
	    public static final int FUNC_LOGON = 2;             // 登录
	    public static final int FUNC_FAST_FIND_SEAT = 3;    // 快速游戏
	    public static final int FUNC_READY_PLAY = 4;        // 准备就绪

	    public static final int FUNC_OTHER_PLAYER_ADD = 30;  // 其他玩家: 加入
	    public static final int FUNC_OTHER_PLAYER_EXIT = 31; // 其他玩家: 退出
	    public static final int FUNC_OTHER_PLAYER_READY = 32; // 其他玩家: readyPlay
}
