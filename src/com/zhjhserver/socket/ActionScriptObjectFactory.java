package com.zhjhserver.socket;

import java.nio.charset.Charset;

import org.apache.mina.core.session.IoSession;
import org.apache.mina.filter.codec.ProtocolCodecFactory;
import org.apache.mina.filter.codec.ProtocolDecoder;
import org.apache.mina.filter.codec.ProtocolEncoder;

public class ActionScriptObjectFactory implements ProtocolCodecFactory {
	private final ActionScriptEncoder encoder;
	private final ActionScriptDecoder decoder;
	public ActionScriptObjectFactory() {
		this(Charset.defaultCharset());
	}
	public ActionScriptObjectFactory(Charset charSet) {
		this.encoder = new ActionScriptEncoder(charSet);
		this.decoder = new ActionScriptDecoder(charSet);
	}
	@Override
	public ProtocolDecoder getDecoder(IoSession session) throws Exception {
		return decoder;
	}

	@Override
	public ProtocolEncoder getEncoder(IoSession session) throws Exception {
		return encoder;
	}

}