package com.zhjhserver.database;

import java.sql.*;
import java.util.Random;

import net.sf.json.JSONObject;

import com.zhjhserver.utils.Const;
import com.zhjhserver.utils.PubFunc;

public class UserDb extends MyDb
{
    public interface Status
    {
        public static final int UNRIPE = 0;     // 未准备
        public static final int IDLE = 1;       // 在观看(已就座)
        public static final int READY = 2;      // 已准备
        public static final int PLAY = 3;       // 正在玩中
    }

    private static Object getValue(String username, String fieldName)
    {
    	if (!isOpen())
        {
            return null;
        }

        try {
            String sql = String.format(
                "SELECT %s FROM %s WHERE username='%s'"
                , fieldName, TB_USER, username);
            ResultSet rs = statement.executeQuery(sql);
            Object r = null;

            if ( rs.next() )
            {
            	r = rs.getObject(fieldName);
            }

            rs.close();

            return r;
        } catch(SQLException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    private static boolean setValue(String username, String fieldName, Object value)
    {
        if (!isOpen())
        {
            return false;
        }

        try {
            String sql = String.format(
                "UPDATE %s SET %s='%s' WHERE username='%s'"
            	, TB_USER, fieldName, value, username);
            statement.execute(sql);
            return true;
        } catch(SQLException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    public static ResultSet getInfo(String username)
    {
    	if (!isOpen())
        {
            return null;
        }

        try {
            String sql = String.format(
                "SELECT * FROM %s WHERE username='%s'"
                , TB_USER, username);
            ResultSet rs = statement.executeQuery(sql);
            return rs;
        } catch(SQLException e) {
            e.printStackTrace();
        } catch(Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public static void setLogon(String username)
    {
        String last_time = PubFunc.getNowTime();
        setValue(username, "last_time", last_time);
    }

    public static boolean exists(String username)
    {
        String r = (String)getValue(username, "username");

        return (r != null);
    }

    public static Integer getScore(String username)
    {
        return (Integer)getValue(username, "score");
    }

    public static Integer getTableNo(String username)
    {
        return (Integer)getValue(username, "table_no");
    }

    public static Integer getStatus(String username)
    {
    	return (Integer)getValue(username, "status");
    }

    public static int register(String username, String passwd, String deviceId, String deviceModel)
    {
        int error = Const.ERROR_UNKOWN;

        if (!isOpen())
        {
            return Const.ERROR_DATABASE_QUERY;
        }

        if (exists(username))
        {
            return Const.ERROR_NAME_EXISTED;
        }

        String timeNow = PubFunc.getNowTime();
        String sql = String.format(
            "INSERT INTO %s(username, passwd, device, model, create_time) VALUES ('%s', '%s', '%s', '%s', '%s')"
            , TB_USER, username, passwd, deviceId, deviceModel, timeNow);

        try {
            statement.execute(sql);
            error = Const.ERROR_SUCCESS;
        } catch(SQLException e) {
            e.printStackTrace();
            error = Const.ERROR_DATABASE_QUERY;
        } catch(Exception e) {
            e.printStackTrace();
        }

        return error;
    }

    public static JSONObject autoMallocAccount(String deviceID, String deviceModel)
    {
        int error = Const.ERROR_SUCCESS;
        String passwd = null;
        String username = null;

        // 先从数据库中查找这台机器原先注册过的帐号
        try {
            String sql = String.format(
                "SELECT username,passwd FROM %s WHERE device='%s'"
                , TB_USER, deviceID);
            ResultSet rs = statement.executeQuery(sql);

            if ( rs.next() )
            {
            	username = rs.getString("username");
            	passwd = rs.getString("passwd");
            }

            rs.close();
        } catch(SQLException e) {
            e.printStackTrace();
        }

        if (username == null)
        {
            // 自动生成
            passwd = makeRandomPasswd();

            do
            {
                username = makeRandomUsername();
                error = register(username, passwd, deviceID, deviceModel);
            } while ( error == Const.ERROR_NAME_EXISTED );
        }

        JSONObject obj = new JSONObject();
        obj.put("error", error);
        obj.put("username", username);
        obj.put("passwd", passwd);

        return obj;
    }

    private static String makeRandomPasswd()
    {
        // 6位数字
        final int NUM = 6;
        Random r = new Random();
        StringBuilder sb = new StringBuilder();

        for (int i=0; i<NUM; i++)
        {
            int n = Math.abs(r.nextInt()) % 10;
            sb.append(n);
        }

        return sb.toString();
    }

    private static String makeRandomUsername()
    {
        // 3位字母+3位数字
        // 0123456789
        // abcdefghijkmnpqrstuvwxyz
        // -: lo

        final int A_NUM = 3;
        final int D_NUM = 3;
        final String AS = "abcdefghijkmnpqrstuvwxyz";
        final int AS_LEN = AS.length();

        StringBuilder sb = new StringBuilder();

        Random r = new Random();

        for (int i=0; i<A_NUM; i++)
        {
            int n = Math.abs(r.nextInt()) % AS_LEN;
            sb.append(AS.charAt(n));
        }

        r = new Random();

        for (int i=0; i<D_NUM; i++)
        {
            int n = Math.abs(r.nextInt()) % 10;
            sb.append(n);
        }

        return sb.toString();
    }
}
